/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mati
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
      
        Singleton s1 = Singleton.getInstance();
        System.out.println("S1 previous set:" + s1.getA());
        s1.setA(10);
        System.out.println("S1 after set:" + s1.getA());
        
        Singleton s2 = Singleton.getInstance();
        System.out.println("S2:" + s2.getA());
        
    }
    
}
