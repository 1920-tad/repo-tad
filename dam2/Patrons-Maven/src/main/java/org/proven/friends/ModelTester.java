package org.proven.friends;

import java.util.List;
import org.proven.friends.model.Friend;
import org.proven.friends.model.Model;

/**
 * Test Model friends
 * 
 * @author Proven
 */
public class ModelTester {
    
    // Message test Ok
    private String msgPass;
    
    // Message test fail
    private String msgFail;
    
    // model for testing
    Model modelTest;
    
    public ModelTester() {
        msgPass = "Ok";
        msgFail = "Fail";
        modelTest = new Model();
    }

    public Model getModelTest() {
        return modelTest;
    }

    public void setModelTest(Model modelTest) {
        this.modelTest = modelTest;
    }

        
    
    
    /**
     * prompt message to user
     * @param msg to shown
     */
    public void alert(String msg) {
        System.out.println(msg);
    }
    
    /**
     * asserts equality of 2 objects
     * @param result
     * @param expected 
     */
    public void assertEquals(Object result,
                             Object expected) {
       if(result.equals(expected)) {
           alert(msgPass);
       }else{
           alert(msgFail);
       }
    }
    
    /**
     * check that result is null
     * @param result to check
     */
    public void assertNull(Object result) {
       if(result == null) {
           alert(msgPass);
       }else{
           alert(msgFail);
       } 
    }
    
    /**
     * check that result is equals to value expected
     * @param result to check
     */
    public void assertValueInt(int result
                            , int expected) {
       if(result == expected) {
           alert(msgPass);
       }else{
           alert(msgFail);
       } 
    }
    
    /**
     * check that friend exists in data source
     * @param friend 
     */
    public void findExistingFriendByPhone(Friend friend) {
        
        Friend result 
                = getModelTest().findByPhone(
                            friend.getPhone());
        if(result == null) {
            alert(msgFail);
        }else{
            assertEquals(result,friend);
        }
    }
    
    /**
     * Test Find by name
     * @param search name to search
     * @param numResult number of registers result
     */
    public void findByName(String search, int numResult) {
        System.out.println("Test Find by Name");
        modelTest = new Model();
        List<Friend> result = modelTest.findByName(search);
        int numResultSearch 
                = (result == null) ? -1: result.size(); 
        
        /*if(result == null) {
            numResultSearch = -1
        } else {
           numResultSearch = result.size(); 
        }*/
        assertEquals(numResult,numResultSearch);
    }
          
    /**
     * Test Add Existing Friend 
     * @param friend to add
     */
    public void addExistingFriend(Friend friend) {
        System.out.println("Test add Existing Friend");
        modelTest = new Model();
        int result = modelTest.add(friend);
        
        assertEquals(result, 0 );
    }
    
    /**
     * Test add Non existing Friend
     * @param friend 
     */
    public void addNonExistingFriend(Friend friend) {
        System.out.println("Test add Non Existing Friend");
        modelTest = new Model();
        int result = modelTest.add(friend);
        assertEquals(result, 1);        
    }
    
    
    public static void main(String argv[]) {
        ModelTester tester = new ModelTester();
        
        tester.addExistingFriend(new Friend("Harry",30,"111"));
        tester.addExistingFriend(new Friend("Bou",30,"222"));
        tester.addNonExistingFriend(new Friend("Leo",30,"555"));
        tester.addNonExistingFriend(new Friend("Poe",30,"666"));
        
        tester.findByName("brian",2);
        tester.findByName("Brian",2);
        
        tester.findExistingFriendByPhone(new Friend("333"));
        tester.findExistingFriendByPhone(new Friend("666"));
        // test de find Non existing Friend By Phone
        // tester.findNonExistingFriendByPhone(new Friend("666"));
    }

   

   
    
    
}
