package org.proven.friends.model.persist;

import java.util.List;
import org.proven.friends.model.Friend;

/**
 * @author Proven
 */
public interface FriendDaoInterface {
    
    /**
     * Add friend to data repository
     * @param friend
     * @return 1 friend added
     *         0 friend not added or otherwise
     */
    int add(Friend friend);
    
    /**
     * detele friend from data repository
     * @param friend
     * @return 1 friend deleted
     *         0 friend not deleted or otherwise
     */
    int delete(Friend friend);
    
    /**
     * Modify friend from data repository
     * @param oldFriend friend to 
     * @param newFriend data friend modified
     * @return 1 friend modified
     *         0 friend not modified or otherwise
     */
    int update(Friend oldFriend, Friend newFriend);
    
    /**
     * Frind a equal friend to data Repository
     * @param friend 
     * @return Friend searched if equals
     *         null if not find
     */
    Friend find(Friend friend);
    
    /**
     * List all friends from Repository
     * @return all data
     */
    List<Friend> findAll();
    
    /**
     * Search list of friends by same name 
     * @param name to search
     * @return list with given name
     */
    List<Friend> findByName(String name);
    
    
}
