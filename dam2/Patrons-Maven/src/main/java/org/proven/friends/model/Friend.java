package org.proven.friends.model;

import java.util.Objects;

/**
 *
 * @author Proven
 * ADT Object Friend
 */
public class Friend {
    
    String name;
    int age;
    String phone;
    
    
    /*
     * Constructors
    */

    public Friend() {
        name = "";
        age= 0;
        phone = "";
    }

    public Friend(String name, int age, String phone) {
        this.name = name;
        this.age = age;
        this.phone = phone;
    }

    public Friend(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.phone);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        boolean b = false;
        
        if (obj == null) {  // null object
            b = false;
        }else {
            if (this == obj) { // same object
                b = true;
            }else {
                if(obj instanceof Friend) {
                    Friend object = (Friend) obj;
                    b = (this.phone.equals(object.getPhone()));
                }else {
                    b = false;
                }
            }
        }
        return b;
    }

    @Override
    public String toString() {
        return "Friend{" + "name=" + name 
                + ", age=" + age 
                + ", phone=" + phone + '}';
    }
    
 
}
