package org.proven.friends.model.persist;

import java.util.ArrayList;
import java.util.List;
import org.proven.friends.model.Friend;

/**
 * <strong>FriendArrayDao.java</strong>
 * DAO persistence of Friend in Array
 * @author Proven
 */
public class FriendArrayDao implements FriendDaoInterface {
    
    private final List<Friend> data;
    private static FriendArrayDao instance;
    
    
    private FriendArrayDao() {
        data = new ArrayList<>();
        loadDataTest();
        
    }
    
    public static FriendArrayDao getInstance() {
        if(instance == null) {
            instance = new FriendArrayDao();
        }
        return instance;
    }
    
    public void loadDataTest() {
        data.add(new Friend("John",30,"111"));
        data.add(new Friend("Brian",20,"222"));
        data.add(new Friend("Paul",11,"333"));
        data.add(new Friend("Brian",23,"444"));
    }

    @Override
    public int add(Friend friend) {
        int codeReturn;
        boolean exist = data.contains(friend);
        if(exist) {
            codeReturn = 0;
        }else {
            if(data.add(friend)) {
                codeReturn = 1;
            }else{
                codeReturn = 0;
            }
        }
        return codeReturn;
    }

    @Override
    public int delete(Friend friend) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(Friend oldFriend, Friend newFriend) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Friend find(Friend friend) {
        Friend friendReturn = null;
        boolean trobat = false;
        for(int i=0; i < data.size() && !trobat; i++) {
            Friend f = data.get(i);
            if(friend.equals(f)) {
                trobat = true;
                friendReturn  = f;
            }
        }
        return friendReturn;
    }

    @Override
    public List<Friend> findAll() {
        return data;
    }

    @Override
    public List<Friend> findByName(String name) {
        List<Friend> list = new ArrayList<Friend>();
        /*for(int i=0;i < data.size(); i++) {
            Friend f = data.get(i);
        */
        for(Friend f: data) {
            // if(name.toLowerCase().equals(f.getName().toLowerCase())) {
            if(name.equalsIgnoreCase(f.getName())) {
            //if(name.equals(f.getName())) {
                list.add(f);
            }
        }
        return list;
    }
    
}
