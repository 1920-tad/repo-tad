package org.proven.friends.model;

import java.util.List;
import org.proven.friends.model.persist.FriendArrayDao;
import org.proven.friends.model.persist.FriendDaoInterface;

/**
 *
 * @author Proven
 * 
 * Model Project Friends
 */
public class Model {
    
    FriendDaoInterface dao;

    public Model() {
        dao = FriendArrayDao.getInstance();
        
    }
    
    
    /**
     * add Friend 
     * @param friend to add
     * @return 1 Friend added 0 not added or otherwise
     */
    public int add(Friend friend) {
        return dao.add(friend);
    }
    
    /**
     * Delete Friend from data source
     * @param friend to delete
     * @return 0 if not exist or not deleted or otherwise
     *         1 friend deleted  
     */
    public int delete(Friend friend) {
        return 0;
    }
    
    /**
     * Modify Friend from Data Source
     * @param friend to modify
     * @return 0 if not modified or otherwise
     *         1 if Friend is modified  
     */
    public int modify(Friend friend) {
        return 0;
    }
    
    /**
     * Modify Friend from Data Source
     * @param oldFriend friend to search in data source
     * @param newFriend data friend to modify
     * @return 0 if not modified or otherwise
     *         1 if Friend is modified  
     */
    public int modify(Friend oldFriend
                      , Friend newFriend) {
        return 0;        
    }
    
    /**
     * get all list of Frinds from data source
     * @return List of Friends empty, 1 friend or multiple
     *         null error or otherwise
     */
    public List<Friend> listAll() {
        return dao.findAll();
    }
    
    /**
     * find a Friend by Phone
     * @param phone to search 
     * @return friend with equals phone
     *         null friend not exists in data source 
     *              or otherwise  
     */
    public Friend findByPhone(String phone) {
        Friend f = new Friend(phone);
        return dao.find(f);
    }
    
    /**
     * find friends with name equals 
     * @param name to search 
     * @return List of Friends empty, 1 friend or multiple
     *         null error or otherwise
     */
    public List<Friend> findByName(String name) {
        return dao.findByName(name);
    } 
    
}
