public class Singleton {
    
    private Integer a;
    private static Singleton singleton;
    
    
    private Singleton() {
        a = new Integer(5);
    }
    
    public static Singleton getInstance() {
        if(singleton == null) {
            singleton = new Singleton();
        }
        return singleton;        
    }

    public int getA() {
        return a.intValue();
    }
    
    
    public void setA(int b) {
        a =  new Integer(b);
    }
    
    
    
    
}
