package proven.friends.model.persist;

import java.util.ArrayList;
import java.util.List;

import proven.friends.model.Friend;

/**
 * <strong>FriendArrayDao.java</strong>
 * DAO class for Friend persistence in an array.
 * Implements singleton pattern.
 * @author ProvenSoft
 */
public class FriendArrayDao implements FriendDaoInterface {
    
    private static FriendArrayDao instance;
    private final List<Friend> data;
    
    private FriendArrayDao()  {
        data = new ArrayList<>(); 
        loadTestData();  //TODO: remove test data for production.
    }   

    public static FriendArrayDao getInstance(){
      if (instance == null) {
           instance = new FriendArrayDao();
      }
      return instance;
    }
    
    @Override
    public Friend find(Friend entity) {
        Friend entityFound;
        int index = data.indexOf(entity);
        if (index >= 0) {
            entityFound = data.get(index);
        }
        else {
            entityFound = null;
        }
        return entityFound;    
    }

    @Override
    public int insert(Friend entity) {
        int rowsAffected;
        boolean alreadyExists = data.contains(entity);
        if (alreadyExists) {
            rowsAffected = 0;
        }
        else {
            boolean success = data.add(entity);
            if (success) rowsAffected = 1;
            else rowsAffected = 0;
        }
        return rowsAffected;    
    }

    @Override
    public int update(Friend oldEntity, Friend newEntity) {
        //TODO avoid phone duplicates.
        int rowsAffected;
        int index = data.indexOf(oldEntity);
        if (index >= 0) {
            data.set(index, newEntity);
            rowsAffected = 1;
        }
        else {
            rowsAffected = 0;
        }
        return rowsAffected;    
    }

    @Override
    public int delete(Friend entity) {
        int rowsAffected;
        if (data.contains(entity)) {
            data.remove(entity);
            rowsAffected = 1;
        }
        else {
            rowsAffected = 0;
        }
        return rowsAffected;    
    }

    @Override
    public List<Friend> findAll() {
        return (List<Friend>) data;
    }
        
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Friend f : data) {
            sb.append(f.toString());
        }
        return sb.toString();
    }
    
    public final void loadTestData() {
        data.add(new Friend("111", "Peter1", 1));
        data.add(new Friend("112", "Peter2", 2));
        data.add(new Friend("113", "Peter3", 3));
        data.add(new Friend("114", "Peter2", 4));
        data.add(new Friend("115", "Peter5", 5));
    }

    @Override
    public List<Friend> findByName(String name) {
        List<Friend> found = new ArrayList<>();
        for (int i=0; i<data.size(); i++) {
            Friend f = data.get(i);
            if (f.getName().equals(name)) {
                found.add(f);
            }
        }
        return found;
    }
    
}