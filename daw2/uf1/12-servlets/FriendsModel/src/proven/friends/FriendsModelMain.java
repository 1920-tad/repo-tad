package proven.friends;

import proven.friends.model.FriendModel;
import proven.friends.model.Friend;

/**
 * <strong>FriendsModelMain.java</strong>
 * Application to manage friends and categories. uses the
 * {@link proven.friends.controllers.FriendController} class.
 *
 * @author Jose Moreno
 */
public class FriendsModelMain {

    public static void main(String[] args) {
        FriendModel model = new FriendModel();

        model.add(new Friend("111", "Susan", 21));
        model.add(new Friend("222", "Peter", 21));
        model.add(new Friend("333", "Alana", 21));
        System.out.println(model.toString());
        model.modify(new Friend("111", "", 0), new Friend("444", "Carol", 32));
        System.out.println(model.toString());
        model.remove(new Friend("444", "", 0));
        System.out.println(model.toString());
        Friend f = model.find(new Friend("222", "", 0));
        System.out.println(f.toString());
    }
}