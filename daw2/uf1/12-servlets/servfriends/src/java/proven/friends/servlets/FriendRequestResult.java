package proven.friends.servlets;

public class FriendRequestResult {
    private Object data;
    private int resultCode;

    public FriendRequestResult(Object data, int resultCode) {
        this.data = data;
        this.resultCode = resultCode;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    @Override
    public String toString() {
        return "FriendRequestResult{" + "data=" + data + ", resultCode=" + resultCode + '}';
    }
    
}