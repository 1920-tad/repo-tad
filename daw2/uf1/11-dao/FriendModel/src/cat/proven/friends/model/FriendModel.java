package cat.proven.friends.model;

import cat.proven.friends.model.persist.FriendDaoInterface;
import cat.proven.friends.model.persist.FriendDaoList;
import java.util.List;

/**
 * <strong>FriendModel.java</strong>
 * MODEL class for Friends .
 * @author ProvenSoft
 */
public class FriendModel {
    
    private FriendDaoInterface friendDao;
    
    public FriendModel() {
        friendDao = new FriendDaoList();
    }
    /**
     * finds a friend in the data source.
     * @param entity to select
     * @return the friend found or null if not found or in case of error.
     */
    public Friend find(Friend entity) {
        return friendDao.select(entity);
    }
    /**
     * adds a new friend to the data source.
     * @param entity to add
     * @return 1 if successfully added, 0 otherwise
     */
    public int add(Friend entity) {
        return friendDao.insert(entity);
    }
    /**
     * 
     * @param oldEntity old version to modify
     * @param newEntity new data for object to be modified
     * @return 1 if successfully modified, 0 otherwise
     */
    public int modify(Friend oldEntity, Friend newEntity) {
        return 0;
    }
    /**
     * removes a friend from the data source.
     * @param entity ro remove
     * @return 1 if successfully removed, 0 otherwise.
     */
    public int remove(Friend entity) {
        return friendDao.delete(entity);
    }
    /**
     * gets all friends from the data source.
     * @return a list with all data, an empty list if none is found or null in case of error.
     */
    public List<Friend> findAll() {
        return friendDao.selectAll();
    }

    /**
     * gets friends from the data source with the given name.
     * @param name the name to search.
     * @return a list with all data, an empty list if none is found or null in case of error.
     */    
    public List<Friend> findFriendsByName(String name) {
        return null;
    }
    
}