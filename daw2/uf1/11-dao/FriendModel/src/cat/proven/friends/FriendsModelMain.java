package cat.proven.friends;

import cat.proven.friends.model.FriendModel;
import cat.proven.friends.model.Friend;
import java.util.List;

/**
 * <strong>FriendsModelMain.java</strong>
 * Application to manage friends and categories. uses the
 * {@link proven.friends.controllers.FriendController} class.
 *
 * @author ProvenSoft
 */
public class FriendsModelMain {

    public static void main(String[] args) {
        FriendModel model = new FriendModel();

//        model.add(new Friend("111", "Susan", 21));
//        model.add(new Friend("222", "Peter", 21));
//        model.add(new Friend("333", "Alana", 21));
//        displayFriendList(model.findAll());
//        model.modify(new Friend("111", "", 0), new Friend("444", "Carol", 32));
//        displayFriendList(model.findAll());
//        model.remove(new Friend("444", "", 0));
//        System.out.println(model.findAll());
//        Friend f = model.find(new Friend("222", "", 0));
//        System.out.println(f.toString());
        System.out.println("Display all");
        List<Friend> allFriends = model.findAll();
        displayFriendList(allFriends);
        System.out.println("Remove 113");
        model.remove(new Friend("113"));
        System.out.println("Display all");
        allFriends = model.findAll();
        displayFriendList(allFriends);
        System.out.println("Search 114");
        Friend f1 = model.find(new Friend("114"));
        System.out.println(f1.toString());
    }
    
    /**
     * displays list of friend.
     * @param data list to print.
     */
    public static void displayFriendList(List<Friend> data) {
        if (data == null) {
            System.out.println("Null data");
        } else {
            for (Friend f: data) {
                System.out.println(f.toString());
            }
        }
    }
    
    
    
    
    
    
}