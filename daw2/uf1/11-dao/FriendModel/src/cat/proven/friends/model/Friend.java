package cat.proven.friends.model;

import java.util.Objects;

/**
 * <strong>Friend.java</strong>
 * ADT for a friend.
 */
public class Friend {
    /**
     * the phone
     */
    private String phone;
    /**
     * the name
     */
    private String name;
    /**
     * the age
     */
    private int age;

    public Friend(String phone, String name, int age) {
        this.phone = phone;
        this.name = name;
        this.age = age;
    }

    public Friend() {
    }
    
    public Friend(String phone) {
        this.phone = phone;
    }  

    public Friend(Friend other) {
        this.phone = other.phone;
        this.name = other.name;
        this.age = other.age;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.phone);
        return hash;
    }

    /**
     * Compares two friends. 
     * Two friends are considered equals if their phones are equals.
     * @param obj is the friend to compare with
     * @return true if friends are equal to each other, false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        boolean b = false;
        if (obj==null) { //null object
            b = false;
        } else {
            if (this==obj) {  //same object
                b = true;
            } else {
                if (obj instanceof Friend) {
                    Friend other = (Friend) obj;
                    b = (this.phone.equals(other.phone));
                } else {
                    b = false;
                }
            }
        }
        return b;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Friend{");
        sb.append("[phone="); sb.append(phone);
        sb.append("][name="); sb.append(name);
        sb.append("][age="); sb.append(age);
        sb.append("]}");
        return sb.toString();
    }
    
}