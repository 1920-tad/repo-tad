package cat.proven.friends.model.persist;

import cat.proven.friends.model.Friend;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ProvenSoft
 */
public class FriendDaoList implements FriendDaoInterface {

    //attributes
    private List<Friend> data;
    
    //constructor.

    public FriendDaoList() {
        data = new ArrayList<>();
        loadTestData(); //load initial test data, remove for production.
    }
    
    //methods.
    
    @Override
    public Friend select(Friend entity) {
        Friend result;
        int index = data.indexOf(entity);
        if (index >= 0) {
            result = data.get(index);
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public int insert(Friend entity) {
        int rowsAffected;
        boolean alreadyExists = data.contains(entity);
        if (alreadyExists) {
            rowsAffected = 0;
        }
        else {
            boolean success = data.add(entity);
            if (success) rowsAffected = 1;
            else rowsAffected = 0;
        }
        return rowsAffected;
    }

    @Override
    public int delete(Friend entity) {
        int rowsAffected;
        //check object existance.
        if (data.contains(entity)) {
            rowsAffected = data.remove(entity) ? 1 : 0;
        } else {
            rowsAffected = 0;
        }
        return rowsAffected;
    }

    @Override
    public int update(Friend oldEntity, Friend newEntity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Friend> selectWhereName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * load initial test data,
     * remove for production.
     */
    public final void loadTestData() {
        data.add(new Friend("111", "Peter1", 1));
        data.add(new Friend("112", "Peter2", 2));
        data.add(new Friend("113", "Peter3", 3));
        data.add(new Friend("114", "Peter2", 4));
        data.add(new Friend("115", "Peter5", 5));
        data.add(new Friend("116", "Peter2", 6));
    }    

    @Override
    public List<Friend> selectAll() {
        return data;
    }
    
    
}
