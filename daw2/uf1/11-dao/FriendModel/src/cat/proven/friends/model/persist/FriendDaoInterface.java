package cat.proven.friends.model.persist;

import cat.proven.friends.model.Friend;
import java.util.List;

/**
 *
 * @author ProvenSoft
 */
public interface FriendDaoInterface {
    /**
     * searches entity in the data source.
     * @param entity the object to search
     * @return entity found or null if not found or in case of error.
     */
    Friend select(Friend entity);
    
    /**
     * inserts a new element to the data source.
     * @param entity the object to insert.
     * @return number of objects inserted.
     */
    int insert(Friend entity);
    
    /**
     * deletes an object from the data source.
     * @param entity the object to delete.
     * @return number of objects deleted.
     */
    int delete(Friend entity);
    
    /**
     * updates an object in the data source.
     * @param oldEntity the current version of object.
     * @param newEntity the new version of object.
     * @return number of objectes updated.
     */
    int update(Friend oldEntity, Friend newEntity);
 
    /**
     * finds friends with the given name.
     * @param name the name to search.
     * @return a list of friend with the given name, an empty list if none is found 
     * or null in case of error.
     */
    List<Friend> selectWhereName(String name);
    
    /**
     * finds all friends in the data source.
     * @return a list with all friends or null in case of error.
     */
    List<Friend> selectAll();
    
}
