
package cat.proven.friends;

import cat.proven.friends.model.Friend;
import cat.proven.friends.model.FriendModel;
import java.util.List;

/**
 *
 * @author ProvenSoft
 */
public class FriendsModelTester {
    /**
     * passing test message
     */
    private final String msgPass;
    /**
     * failing test message
     */
    private final String msgFail;

    public FriendsModelTester() {
        msgPass = "Pass";
    	msgFail = "Fail";
    }    
    
    /**
     * prompt a message to user
     * @param msg the message to prompt
     */
    private void alert(String msg) {
    	System.out.println(msg);
    }
    
    /**
     * asserts equality of two objects and prompts result to user
     * @param resultValue the value obtained after test operation
     * @param expectedValue the expected value 
     */
    private void assertEquals(Object resultValue, Object expectedValue) 
    {
    	if (expectedValue.equals(resultValue)) {
            alert(this.msgPass);
        } else {
            alert(this.msgFail);
        }	
    }

    /**
     * asserts null value of resultValue and prompts to user
     * @param resultValue the value obtained after test operation
     */
    private void assertNull(Object resultValue) 
    {
    	if (resultValue==null) {
            alert(this.msgPass);
        } else {
            alert(this.msgFail);
        }	
    }    
    
    public void testFindExisting(Friend obj) {
        System.out.println("Test: testFindExisting");
        FriendModel instance = new FriendModel();
        Friend resultValue = instance.find(obj);
        Friend expectedValue = obj;
        assertEquals(resultValue, expectedValue);
    }
    
    public void testFindNotExisting(Friend obj) {
        System.out.println("Test: testFindNotExisting");
        FriendModel instance = new FriendModel();
        Friend resultValue = instance.find(obj);
        assertNull(resultValue);
    }
    
    public void testAddNotExisting(Friend obj) {
        System.out.println("Test: testAddNotExisting");
        FriendModel instance = new FriendModel();
        int resultValue = instance.add(obj);
        int expectedValue = 1;
        assertEquals(resultValue, expectedValue);
    }
    
    public void testAddExisting(Friend obj) {
        System.out.println("Test: testAddExisting");
        FriendModel instance = new FriendModel();
        int resultValue = instance.add(obj);
        int expectedValue = 0;
        assertEquals(resultValue, expectedValue);
    }
    
    public void testRemoveExisting(Friend obj) {
        System.out.println("Test: testRemoveExisting");
        FriendModel instance = new FriendModel();
        int resultValue = instance.remove(obj);
        int expectedValue = 1;
        assertEquals(resultValue, expectedValue);
    }

    public void testRemoveNotExisting(Friend obj) {
        System.out.println("Test: testRemoveNotExisting");
        FriendModel instance = new FriendModel();
        int resultValue = instance.remove(obj);
        int expectedValue = 0;
        assertEquals(resultValue, expectedValue);
    }
    
    public void testFindFriendsByExistingName(String name, int numResults) {
        System.out.println("Test: testFindFriendsByExistingName");
        FriendModel instance = new FriendModel();
        List<Friend> result = instance.findFriendsByName(name);
        int resultValue = (result==null) ? -1 : result.size();
        assertEquals(resultValue, numResults);
    }

    public void testFindFriendsByNotExistingName(String name) {
        System.out.println("Test: testFindFriendsByNotExistingName");
        FriendModel instance = new FriendModel();
        List<Friend> result = instance.findFriendsByName(name);
        int resultValue = (result==null) ? -1 : result.size();
        assertEquals(resultValue, 0);
    }
    
    public static void main(String[] args) {
	FriendsModelTester tester = new FriendsModelTester();
        tester.testFindExisting(new Friend("111", "Peter1", 11));
        tester.testFindNotExisting(new Friend("999", "ZZ", 99));
        tester.testAddNotExisting(new Friend("999", "ZZ", 99));
        tester.testAddExisting(new Friend("111", "Peter1", 11));
        tester.testRemoveExisting(new Friend("111", "Peter1", 11));
        tester.testRemoveNotExisting(new Friend("999", "ZZ", 99));
        tester.testFindFriendsByExistingName("Peter1", 1);
        tester.testFindFriendsByNotExistingName("ZZ");
    }
    
}
