package cat.proven.messager.model.persist;

import cat.proven.messager.model.User;
import java.util.List;

/**
 *
 * @author ProvenSoft
 */
public interface UserDaoInterface {

    /**
     * deletes a user.
     * @param user the user to update
     * @return number of rows affected.
     */
    int delete(User user);

    /**
     * inserts a new user into data store.
     *
     * @param user the user to add
     * @return number of rows affected.
     */
    int insert(User user);

    /**
     * selects users with the given fullname.
     * @param fullname the fullname to search.
     * @return a list of users with the given fullname or null in case of error.
     */
    List<User> selectWhereFullname(String fullname);

    /**
     * selects a user with the given id.
     * @param id the id of user to select.
     * @return user with the given id or null if not found or in case of error.
     */
    User selectWhereId(long id);

    /**
     * selects a user with the given nick.
     * @param nick the nick of user to select.
     * @return user with the given nick or null if not found or in case of error.
     */
    User selectWhereNick(String nick);

    /**
     * selects a user with the given nick and password.
     * @param nick the nick of user to select.
     * @param password the password of user to select.
     * @return user with the given nick or null if not found or in case of error.
     */
    User selectWhereNickAndPassword(String nick, String password);

    /**
     * updates a user.
     * @param user the user to update
     * @return number of rows affected.
     */
    int update(User user);
    
}
