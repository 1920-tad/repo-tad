package cat.proven.messager.model;

import cat.proven.messager.model.persist.MessageDaoInterface;
import cat.proven.messager.model.persist.MessageDaoList;
import cat.proven.messager.model.persist.UserDaoInterface;
import cat.proven.messager.model.persist.UserDaoList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ProvenSoft
 */
public class Model {
    /**
     * user persistence helper.
     */
    private final UserDaoInterface userDao;
    /**
     * message persistence helper.
     */
    private final MessageDaoInterface messageDao;

    public Model() {
        userDao = new UserDaoList();
        messageDao = new MessageDaoList();
    }
    
    /**
     * adds a new user to data store.
     * @param user the user to add
     * @return true if successfully added, false otherwise.
     */
    public boolean addUser(User user) {
        boolean result = false;
        int nRows = userDao.insert(user);
        result = (nRows>0);
        return result;
    }
    
    /**
     * modifies a user in the data store.
     * @param user the user to modify.
     * @return true if successfully modified, false otherwise.
     */
    public boolean modifyUser(User user) {
        boolean result = false;
        int nRows = userDao.update(user);
        result = (nRows>0);
        return result; 
    }

    /**
     * removes a user in the data store.
     * @param user the user to modify.
     * @return true if successfully modified, false otherwise.
     */
    public boolean removeUser(User user) {
        boolean result = false;
        int nRows = userDao.delete(user);
        result = (nRows>0);
        return result;
    }      
    
    /**
     * retrieves user with a given nick.
     * @param nick the nick to search.
     * @return the user with the given nick o null if not found or in case of error.
     */
    public User findUserByNick(String nick) {
        User result = null;
        result = userDao.selectWhereNick(nick);
        return result;
    }

    /**
     * checks user's credentials.
     * @param nick the nick of the user.
     * @param password the password of the user.
     * @return true if credentials are ok false otherwise.
     */
    public boolean checkUserCredentials(String nick, String password) {
        boolean result = false;
        User found = userDao.selectWhereNickAndPassword(nick, password);
        result = !(found == null); 
        return result;
    }
    
    /**
     * searches users with the given fullname.
     * @param fullname the fullname to search.
     * @return a list of users with the given fullname or null in case of error.
     */
    public List<User> findUsersByFullname(String fullname) {
        List<User> result = new ArrayList<>();
        result = userDao.selectWhereFullname(fullname);
        return result;
    }
    
    /**
     * adds a new message to data store.
     * @param message the message to add.
     * @param user the user owning the message.
     * @return true if successfully added, false otherwise.
     */
    public boolean addMessage(Message message, User user) {
        boolean result = false;
        //TODO check user existence.
        boolean userExists = true;
        if (userExists) {
            message.setOwner(user);
            int nRows = messageDao.insert(message);
            result = (nRows>0);       
        }
        return result;
    }    
    
    /**
     * modifies a message in data store.
     * @param message the message to modify with the new values.
     * @return true if successfully modified, false otherwise.
     */
    public boolean modifyMessage(Message message) {
        boolean result = false;
        int nRows = messageDao.update(message);
        result = (nRows>0);
        return result;        
    }

    /**
     * removes a message in data store.
     * @param message the message to remove with the new values.
     * @return true if successfully removed, false otherwise.
     */
    public boolean removeMessage(Message message) {
        boolean result = false;
        int nRows = messageDao.delete(message);
        result = (nRows>0);
        return result;        
    }
    
    /**
     * retrieves messages of a given user.
     * @param user the user whose message are to be retrieved.
     * @return a list of messages of the given user or null in case of error.
     */
    public List<Message> findMessagesByUser(User user) {
        List<Message> result = null;
        result = messageDao.selectWhereOwner(user);
        return result;
    }
    
    /**
     * retrieves messages whose title contains a given text.
     * @param title the title fragment to search.
     * @return a list of messages or null in case of error.
     */
    public List<Message> findMessagesLikeTitle(String title) {
        List<Message> result = null;
        result = messageDao.selectLikeTitle(title);
        return result;
    }

    /**
     * retrieves messages whose text contains a given text.
     * @param text the title fragment to search.
     * @return a list of messages or null in case of error.
     */
    public List<Message> findMessagesLikeText(String text) {
        List<Message> result = null;
        result = messageDao.selectLikeTitle(text);
        return result;
    }
    
    /**
     * loads data for testing purposes.
     * Important: remove in production environment.
     */
    public void loadTestData() {
        //load users
        userDao.insert( new User("nick01", "pass01", "fullname01", Role.ADMINISTRATOR) );
        userDao.insert( new User("nick02", "pass02", "fullname02", Role.REGISTERED_USER) );
        userDao.insert( new User("nick03", "pass03", "fullname03", Role.REGISTERED_USER) );
        userDao.insert( new User("nick04", "pass04", "fullname01", Role.REGISTERED_USER) );
        userDao.insert( new User("nick05", "pass05", "fullname01", Role.REGISTERED_USER) );
        //load messages
        messageDao.insert( new Message("title01", "text01", new User(1)) );
        messageDao.insert( new Message("title02", "text02", new User(2)) );
        messageDao.insert( new Message("title03", "text03", new User(3)) );
        messageDao.insert( new Message("title04", "text04", new User(4)) );
        messageDao.insert( new Message("title05", "text05", new User(5)) );
        messageDao.insert( new Message("title06", "text06", new User(1)) );
        messageDao.insert( new Message("title07", "text07", new User(1)) );
        messageDao.insert( new Message("title08", "text08", new User(2)) );
        messageDao.insert( new Message("title09", "text09", new User(3)) );
        messageDao.insert( new Message("title10", "text10", new User(4)) );
    }
}
