package cat.proven.messager;

import cat.proven.messager.model.Message;
import cat.proven.messager.model.Model;
import cat.proven.messager.model.User;
import java.util.List;

/**
 * 
 * @author ProvenSoft
 */
public class Main {

    public static void main(String[] args) {
        Model model = new Model();
        model.loadTestData();
        List<Message> mList = model.findMessagesByUser( new User(1) );
        mList.forEach((t) -> {System.out.println(t.toString()); });
    }
 
}
