package cat.proven.messager.model;

/**
 *
 * @author ProvenSoft
 */
public enum Role {
    ADMINISTRATOR,
    REGISTERED_USER,
    VISITOR
}
