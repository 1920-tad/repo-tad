package cat.proven.messager.model;

/**
 * ADT for User.
 * @author ProvenSoft
 */
public class User {
    
    private long id;
    private String nick;
    private String password;
    private String fullname;
    private Role role;

    public User(long id, String nick, String password, String fullname, Role role) {
        this.id = id;
        this.nick = nick;
        this.password = password;
        this.fullname = fullname;
        this.role = role;
    }

    public User(String nick, String password, String fullname, Role role) {
        this.nick = nick;
        this.password = password;
        this.fullname = fullname;
        this.role = role;
    }    
    
    public User(String nick, String password, String fullname) {
        this.nick = nick;
        this.password = password;
        this.fullname = fullname;
    }

    public User(long id) {
        this.id = id;
    }

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    } 

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", nick=" + nick + ", password=" + password + ", fullname=" + fullname + '}';
    }

}
