package cat.proven.messager.model;

/**
 * ADT for Message.
 * @author ProvenSoft
 */
public class Message {

    private long id;
    private String title;
    private String text;
    private User owner;

    public Message(long id, String title, String text, User owner) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.owner = owner;
    }

    public Message(String title, String text, User owner) {
        this.title = title;
        this.text = text;
        this.owner = owner;
    }    
    
    public Message(long id) {
        this.id = id;
    }

    public Message() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Message other = (Message) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Message{" + "id=" + id + ", title=" + title + ", text=" + text + ", owner=" + owner + '}';
    }
}
