package cat.proven.messager.model.persist;

import cat.proven.messager.model.Message;
import cat.proven.messager.model.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ProvenSoft
 */
public class MessageDaoList implements MessageDaoInterface {

    private static long lastId = 0;
    private final List<Message> messages;

    public MessageDaoList() {
        messages = new ArrayList<>();
    }
    
    public static long getLastId() {
        return lastId;
    }

    /**
     * inserts a new message into data store.
     *
     * @param message the message to add
     * @return number of rows affected.
     */
    @Override
    public int insert(Message message) {
        int result = 0;
        lastId++;
        message.setId(lastId);
        //System.out.println("MessageDao.add: " + message.toString());
        boolean b = messages.add(message);
        result = (b) ? 1 : 0;
        return result;
    }
    
    /**
     * updates a message.
     * @param message the message to update
     * @return number of rows affected.
     */
    @Override
    public int update(Message message) {
        int result = 0;
        int index = messages.indexOf(message);
        if (index>=0) {
            messages.set(index, message);
            result = 1;
        }
        return result;        
    }    

    /**
     * deletes a message.
     * @param message the message to update
     * @return number of rows affected.
     */
    @Override
    public int delete(Message message) {
        int result = 0;
        boolean b = messages.remove(message);
        result = (b) ? 1 : 0;
        return result;        
    }   
    
    /**
     * selects a message with the given id.
     * @param id the id of message to select.
     * @return message with the given id or null if not found or in case of error.
     */
    @Override
    public Message selectWhereId(long id) {
        Message result = null;
        Message m = new Message(id);
        int index = messages.indexOf(m);
        if (index>=0) {
            result = messages.get(index);
        }
        return result;
    }  
    
    /**
     * selects messages with the title containing given text.
     * @param text the text to search in title.
     * @return a list of messages with the given title or null in case of error.
     */
    @Override
    public List<Message> selectLikeTitle(String text) {
        List<Message> result = new ArrayList<>();
        //TODO: perform request.
        return result;
    }
    
    /**
     * selects messages with the text containing given text.
     * @param text the text to search in text.
     * @return a list of messages with the given text or null in case of error.
     */
    @Override
    public List<Message> selectLikeText(String text) {
        List<Message> result = new ArrayList<>();
        //TODO: perform request.
        return result;
    }   
        
    /**
     * selects messages of the given owner.
     * @param owner the value of owner
     * @return a list of messages of given owner.
     */
    @Override
    public List<Message> selectWhereOwner(User owner) {
        List<Message> result = new ArrayList<>();
        messages.stream().filter((m) -> (m.getOwner().equals(owner))).forEachOrdered((m) -> {
            result.add(m);
        });
        return result;
    }       
    
}
