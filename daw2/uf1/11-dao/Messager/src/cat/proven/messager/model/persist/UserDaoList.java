package cat.proven.messager.model.persist;

import cat.proven.messager.model.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ProvenSoft
 */
public class UserDaoList implements UserDaoInterface {

    private static long lastId = 0;
    private final List<User> users;

    public UserDaoList() {
        users = new ArrayList<>();
    }
    
    public static long getLastId() {
        return lastId;
    }

    /**
     * inserts a new user into data store.
     *
     * @param user the user to add
     * @return number of rows affected.
     */
    @Override
    public int insert(User user) {
        int result = 0;
        lastId++;
        user.setId(lastId);
        //System.out.println("UserDao.add: " + user.toString());
        boolean b = users.add(user);
        result = (b) ? 1 : 0;
        return result;
    }
    
    /**
     * updates a user.
     * @param user the user to update
     * @return number of rows affected.
     */
    @Override
    public int update(User user) {
        int result = 0;
        int index = users.indexOf(user);
        if (index>=0) {
            users.set(index, user);
            result = 1;
        }
        return result;        
    }
    
    /**
     * deletes a user.
     * @param user the user to update
     * @return number of rows affected.
     */
    @Override
    public int delete(User user) {
        int result = 0;
        boolean b = users.remove(user);
        result = (b) ? 1 : 0;
        return result;        
    }   
    
    /**
     * selects a user with the given id.
     * @param id the id of user to select.
     * @return user with the given id or null if not found or in case of error.
     */
    @Override
    public User selectWhereId(long id) {
        User result = null;
        User u = new User(id);
        int index = users.indexOf(u);
        if (index>=0) {
            result = users.get(index);
        }
        return result;
    }

    /**
     * selects a user with the given nick.
     * @param nick the nick of user to select.
     * @return user with the given nick or null if not found or in case of error.
     */
    @Override
    public User selectWhereNick(String nick) {
        User result = null;
        //TODO: perform request.
        return result;
    } 
    
    /**
     * selects a user with the given nick and password.
     * @param nick the nick of user to select.
     * @param password the password of user to select.
     * @return user with the given nick or null if not found or in case of error.
     */
    @Override
    public User selectWhereNickAndPassword(String nick, String password) {
        User result = null;
        //TODO: perform request.
        return result;
    }
    
    /**
     * selects users with the given fullname.
     * @param fullname the fullname to search.
     * @return a list of users with the given fullname or null in case of error.
     */
    @Override
    public List<User> selectWhereFullname(String fullname) {
        List<User> result = new ArrayList<>();
        //TODO: perform request.
        return result;
    }
    
}
