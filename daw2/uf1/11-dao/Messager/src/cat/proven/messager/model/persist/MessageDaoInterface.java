package cat.proven.messager.model.persist;

import cat.proven.messager.model.Message;
import cat.proven.messager.model.User;
import java.util.List;

/**
 *
 * @author ProvenSoft
 */
public interface MessageDaoInterface {

    /**
     * deletes a message.
     * @param message the message to update
     * @return number of rows affected.
     */
    int delete(Message message);

    /**
     * inserts a new message into data store.
     *
     * @param message the message to add
     * @return number of rows affected.
     */
    int insert(Message message);

    /**
     * selects messages with the text containing given text.
     * @param text the text to search in text.
     * @return a list of messages with the given text or null in case of error.
     */
    List<Message> selectLikeText(String text);

    /**
     * selects messages with the title containing given text.
     * @param text the text to search in title.
     * @return a list of messages with the given title or null in case of error.
     */
    List<Message> selectLikeTitle(String text);

    /**
     * selects a message with the given id.
     * @param id the id of message to select.
     * @return message with the given id or null if not found or in case of error.
     */
    Message selectWhereId(long id);

    /**
     * selects messages of the given owner.
     * @param owner the value of owner
     * @return a list of messages of given owner.
     */
    List<Message> selectWhereOwner(User owner);

    /**
     * updates a message.
     * @param message the message to update
     * @return number of rows affected.
     */
    int update(Message message);
    
}
